#!/bin/ash

echo "Configuring startup, setting installation to skip wizard and to run on ${PUBLIC_URI}, internal port used: ${PORT}."
/hub/bin/hub.sh configure --listen-address=0.0.0.0 --listen-port=${PORT} -J-Ddisable.configuration.wizard.on.clean.install=true --base-url=${PUBLIC_URI}

echo "Starting Jetbrains Hub."
/hub/bin/hub.sh run --no-browser