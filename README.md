# Jetbrains Hub docker image.

Docker image for jetbrains Hub using the zip version (which in turn enables external hub connection).
The image is based on **Alpine linux** using **OpenJDK**.

Image can be found at [GitLab](https://gitlab.com/jitesoft/dockerfiles/jetbrains-hub).

_Observe: since version 2018.4.11442 the image used as base is the jitesoft/openjdk image, hence the openjdk version has been updated to use 
openjdk 13-eap. If any issues arise from this, please report this as an issue on the gitlab issue tracker for the image._

## Directories worth persisting

The following directories are worth persisting if you wish to store the data even when restarting the 
container:

`/hub/data` - The data directory that is used by Hub.  
`/hub/backups` - Backup directory which backups are stored in.  
`/hub/logs` - Logfiles.  
`/hub/conf` - Config directory.  

The directories are set as `VOLUMES` in the docker file.

## Environment vars

Port 8080 is the default port used by Hub, if you wish to use another port for some reason, 
change the env variable `PORT` to desired value and expose it.

The `PUBLIC_URI` variable should be set to the public URL of the installation (including port and schema).
Defaults to `http://127.0.0.1:8080`.

## Initial startup

When starting Hub for the first time, the installation will run automatically, you can go to `http://127.0.0.1:8080/starting`
to see the progress. When all is ready, the default administration account have the credentials `admin`/`admin`, I recommend
that you change this as soon as possible!

## Tags

Tags are defined by the following standard:

`latest` latest build.  
`year` latest of the `year` builds (i.e., 2018).  
`year.x` latest of the `year-major` builds (i.e., 2018.4).  
`yearh.x.x` latest of the `year.major.minor` builds (i.e., 2018.4.1)
